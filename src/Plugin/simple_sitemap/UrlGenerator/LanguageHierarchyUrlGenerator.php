<?php

namespace Drupal\simple_sitemap_language_hierarchy\Plugin\simple_sitemap\UrlGenerator;

use Drupal\simple_sitemap\Plugin\simple_sitemap\UrlGenerator\EntityUrlGenerator;
use Drupal\Core\Url;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Handle all url generate based on language hierarchy.
 *
 * @package Drupal\simple_sitemap_language_hierarchy\Plugin\simple_sitemap\UrlGenerator
 *
 * @UrlGenerator(
 *   id = "language_hierarchy_url_generator",
 *   label = @Translation("Language Hierarchy Entity URL generator"),
 *   description = @Translation("Generates URLs for entity bundles and bundle overrides."),
 * )
 */
class LanguageHierarchyUrlGenerator extends EntityUrlGenerator {

  /**
   * Gets the alternate URLs for translated languages.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to process.
   * @param \Drupal\Core\Url $url_object
   *   The URL object.
   *
   * @return array
   *   An array of alternate URLs.
   */
  protected function getAlternateUrlsForTranslatedLanguages(ContentEntityInterface $entity, Url $url_object): array {
    $alternate_urls = parent::getAlternateUrlsForTranslatedLanguages($entity, $url_object);

    // Update the function outcome to give the same result as the service.
    $hierarchCache = \Drupal::service('simple_sitemap_language_hierarchy.manager');
    $hierarchyArray = $hierarchCache->getCachedHierarchyArray();
    $excludeLanguages = $this->settings->get('excluded_languages');

    // Get hierarchy form translated languages.
    foreach (array_keys($alternate_urls) as $langCode) {
      foreach (array_keys($hierarchyArray[$langCode]['#children']) as $language) {
        $languageObj = $this->languages[$language];
        if (!isset($excludeLanguages[$language]) && !$languageObj->isDefault()) {
          $custom = $this->replaceBaseUrlWithCustom(
            $url_object
              ->setOption('language', $languageObj)->toString()
          );
          $alternate_urls[$language] = $custom;
        }
      }
    }

    return $alternate_urls;

  }

}
