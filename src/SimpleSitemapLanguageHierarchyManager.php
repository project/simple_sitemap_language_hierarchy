<?php

namespace Drupal\simple_sitemap_language_hierarchy;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\Cache;
use Drupal\language\ConfigurableLanguageInterface;

/**
 * Class LanguageHierarchyManager.
 *
 * @package Drupal\simple_sitemap_language_hierarchy
 */
class SimpleSitemapLanguageHierarchyManager {

  /**
   * Language Hierarchy array Cache.
   *
   * @var string
   */
  const LANGUAGE_HIERARCHY_CACHE_NAME = 'simple_sitemap_language_hierarchy:array';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language storage.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Constructs a TranslationManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   Entity type manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Drupal cache.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_manager,
    CacheBackendInterface $cache
  ) {
    $this->entityTypeManager = $entity_manager;
    $this->cache = $cache;
  }

  /**
   * Get all language parent.
   */
  public function getAllParentLanguage($langCode, $languageConfigEntities = FALSE) {

    // Check lang code.
    if (empty($langCode)) {
      return FALSE;
    }

    $result = &drupal_static(__FUNCTION__);

    if (!empty($result[$langCode])) {
      return $result[$langCode];
    }

    // Initial value.
    $result[$langCode] = [];

    // Get fallback language.
    $languageFallback = $this->getLanguageFallback($langCode, $languageConfigEntities);

    if (empty($languageFallback)) {
      return $result[$langCode];
    }

    $result[$langCode][] = $languageFallback;

    $currentlangCode = $languageFallback;

    while (!empty($languageFallback)) {

      $languageFallback = $this->getLanguageFallback($currentlangCode, $languageConfigEntities);

      if (empty($languageFallback)) {
        return $result[$langCode];
      }

      $result[$langCode][] = $languageFallback;

      $currentlangCode = $languageFallback;
    }

    return $result[$langCode];
  }

  /**
   * Get Languages Fallback.
   */
  public function getLanguageFallback($langCode, $languageConfigEntities = FALSE) {

    if (empty($langCode)) {
      return FALSE;
    }

    $langCodeFallbackParents = &drupal_static(__FUNCTION__);

    if (!empty($langCodeFallbackParents[$langCode])) {
      return $langCodeFallbackParents[$langCode];
    }

    $configManager = $this->entityTypeManager->getStorage('configurable_language');

    // If we already load this language config entity, use it. Else load a new.
    if (!empty($languageConfigEntities[$langCode])) {
      $languageConfigEntity = $languageConfigEntities[$langCode];
    }
    else {
      $languageConfigEntity = $configManager->load($langCode);
    }

    $langCodeFallbackParents[$langCode] = FALSE;
    if (!empty($languageConfigEntity->getThirdPartySetting('language_hierarchy', 'fallback_langcode', ''))) {
      $langCodeFallbackParents[$langCode] = $languageConfigEntity->getThirdPartySetting('language_hierarchy', 'fallback_langcode', '');
    }

    return $langCodeFallbackParents[$langCode];
  }

  /**
   * Get all language hierarchy saved on cache.
   *
   * @return array
   *   All language hierarchy saved on cache.
   */
  public function getCachedHierarchyArray() {
    if ($cache = $this->cache
      ->get(self::LANGUAGE_HIERARCHY_CACHE_NAME)
    ) {
      return $cache->data;
    }

    $data = $this->hierarchyTree();

    $tags = [];
    foreach ($data as $langCode => &$languageData) {
      $tags[] = "hierarchy_array:{$langCode}";

      $configurableLanguage = $languageData['#data'];
      $providers = $configurableLanguage->getThirdPartyProviders();
      $languageData['#settings'] = [];
      foreach ($providers as $provider) {
        $languageData['#settings'] += $configurableLanguage->getThirdPartySettings($provider);
      }

    }

    $this->cache
      ->set(self::LANGUAGE_HIERARCHY_CACHE_NAME, $data, Cache::PERMANENT, $tags);

    return $data;
  }

  /**
   * Returns the children languages of a provided language.
   *
   * @param \Drupal\language\ConfigurableLanguageInterface $language
   *   The language to get the children.
   * @param array $children
   *   The children array to be returned.
   *
   * @return array
   *   Ordered array with all children.
   */
  public function languageHierarchyGetChildren(ConfigurableLanguageInterface $language, array &$children = []): array {
    try {
      $storage = $this->entityTypeManager->getStorage('configurable_language');
      $child_language_ids = $storage->getQuery()
        ->condition('third_party_settings.language_hierarchy.fallback_langcode', $language->getId())
        ->execute();
      foreach ($child_language_ids as $child_language_id) {
        $child_language = $storage->load($child_language_id);
        if ($child_language->getThirdPartySetting('language_hierarchy', 'fallback_langcode')) {
          $this->languageHierarchyGetChildren($child_language, $children);
        }
        $children[$child_language->getId()] = $child_language;
      }
    }
    catch (\Exception $e) {
    }
    return $children;
  }

  /**
   * Returns an array with the ancestors and children of all the languages.
   *
   * @return array
   *   An array with all the languages each one with the ancestors and children.
   *   $result = ['language_code'][
   *      '#ancestors' => 'An array with the ancestors languages',
   *      '#children' => 'An array with the children languages',
   *      '#data' => 'The condigurable language object'
   *   ]
   */
  public function hierarchyTree(): array {
    // Since any language can be a child, load all languages.
    $languages = $this->entityTypeManager->getStorage('configurable_language')->loadMultiple();
    if (empty($languages)) {
      return [];
    }

    $result = [];
    foreach ($languages as $langCode => $language) {
      $ancestors = language_hierarchy_get_ancestors($language);
      $children = $this->languageHierarchyGetChildren($language);
      $result[$langCode] = [
        '#ancestors' => $ancestors,
        '#children' => $children,
        '#data' => $language,
      ];
    }

    return $result;
  }

}
